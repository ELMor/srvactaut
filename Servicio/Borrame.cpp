

#include <windows.h>
#include <nspapi.h>
#include <svcguid.h>
#include <stdio.h>

#define macro(a) (a<0 ? a+256 : a)

int main(int argc, char *argv[]){
	CSADDR_INFO csai[20];
	GUID myguid=SVCID_HOSTNAME;
	unsigned long scsai=sizeof(csai);
	int ret;

	ret=GetAddressByName(
		NS_DEFAULT,
		&myguid,
		argv[1],
		NULL,
		0,
		NULL,
		csai,
		&scsai,
		NULL,
		NULL	);
	printf("%d found\n",ret);
	if(ret){
		printf("%s :: %u.%u.%u.%u\n",
			argv[1],
			macro(csai[0].RemoteAddr.lpSockaddr->sa_data[2]),
			macro(csai[0].RemoteAddr.lpSockaddr->sa_data[3]),
			macro(csai[0].RemoteAddr.lpSockaddr->sa_data[4]),
			macro(csai[0].RemoteAddr.lpSockaddr->sa_data[5])
			);
	}
	return 1;
}
		