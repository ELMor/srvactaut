
#include <stdio.h>
#include "decompile.h"
#include "dump.h"

int BuildCommand(char *jn, char *ns, char *fs, CPUs* cpus, Fics *fic, char *buf){
	if( !cpus ){
		*buf=0;
		return 1;
	}else{
		int j;
		sprintf(buf,"Trabajo  %s\n",jn);				buf+=strlen(buf);
		sprintf(buf,"Notifica %s\n",ns);				buf+=strlen(buf);
		sprintf(buf,"Grupos { \n");						buf+=strlen(buf);
		sprintf(buf," %s { ",fs);						buf+=strlen(buf);
		dumpCPUs( cpus, buf );
		strcat(buf,"} } Ficheros { ");
		buf+=strlen(buf);
		for(j=0;j<fic->carfic;j++){
			sprintf(buf,"\"%s\" { \"%s\" \"%s\" } ",
				fic->fname[j],
				fic->path[j],
				fic->cmd[j] ? fic->cmd[j] : "" );
			buf+=strlen(buf);
		}
		strcat(buf,"}\n");
	}
	return 1;
}

