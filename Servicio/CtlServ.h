/*
 * Fichero CtlServ.h
 */

#ifndef _SERVICE_H
#define _SERVICE_H

#include <windows.h>
#include <stdio.h>
#include <stdlib.h>
#include <process.h>
#include <tchar.h>
#include <sys/stat.h>


#ifdef __cplusplus
extern "C" {
#endif


//////////////////////////////////////////////////////////////////////////////
//// todo: change to desired strings
////
// name of the executable
#define SZAPPNAME            "ActAut"
// internal name of the service
#define SZSERVICENAME        "ActAutService"
// displayed name of the service
#define SZSERVICEDISPLAYNAME "Actualizacion Automatica de Software"
// list of service dependencies - "dep1\0dep2\0\0"
#define SZDEPENDENCIES       ""
#define SIZESENDDATA		 4096
//////////////////////////////////////////////////////////////////////////////



VOID ServiceStart(DWORD dwArgc, LPTSTR *lpszArgv);
VOID ServiceStop();
BOOL ReportStatusToSCMgr(DWORD dwCurrentState, DWORD dwWin32ExitCode, DWORD dwWaitHint);


VOID WINAPI service_ctrl(DWORD dwCtrlCode);
VOID WINAPI service_main(DWORD dwArgc, LPTSTR *lpszArgv);
int  CmdInstallService(char *pszNC, char *progName);
VOID CmdRemoveService(char *pszNC);
VOID CmdRemoveLog(char *pszNC,char *jname);
VOID CmdStartService(char *pszNC);
VOID CmdDebugService(int argc, char **argv);
int  CmdSendJob(char *jobfilename );
BOOL WINAPI ControlHandler ( DWORD dwCtrlType );
LPTSTR GetLastErrorText( LPTSTR lpszBuf, DWORD dwSize );
void RunRemoteCommand(char *pszNC,char cmd[SIZESENDDATA]);
void Ayuda(char *arg);



#ifdef __cplusplus
}
#endif

#endif
