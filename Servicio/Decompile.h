/*
 * Fichero Decompile.h
 */


#ifndef __GROPUS_H__
#define __GROPUS_H__

#include <string.h>
#include <stdio.h>
#include <stdlib.h>

#define nMaxLisAct 16

typedef struct _Fics {
	int  carfic;
	char **fname;
	char **path;
	char **cmd;
	char *delay;
	char *downloaded;
} Fics;

typedef struct _CPU CPU;

typedef struct _CPUs {
	int  ncpu;
	CPU  *cpu_padre;
	CPU **cpu;
} CPUs;

typedef struct _CPU {
	char *cname;
	CPUs *cpus;
} CPU;


typedef struct _LisAct {
	CPU*  cpu;
	Fics* fic;
	char* jobname;
	char* notifysrv;
} LisAct;

int Decompile(char* in, LisAct* th_lis);

void Process(char* in);
int yyparse (void*);

CPU * newCPU (char *Nom=NULL,CPUs* cpu=NULL);
CPU * newCPU (void *Nom=NULL,void* cpu=NULL);
CPUs* newCPUs();
Fics* newFic (void);

void	setLis(LisAct *c, CPU *cpu, Fics *f, char* jname, char* nserver);
void	setLis(LisAct *c, void *cpu, void *f, void* jnom, void* nserver);

CPUs*	addCPUtoCPUs(CPUs *c, CPUs *sc);
CPUs*	addCPUtoCPUs(void *c, void *sc);
void	addFictoFics(Fics *f, char* fn,char *pa, char *cm, int d=0);
void	addFictoFics(void *f, void* fn,void *pa, void *cm);

void FreeAll(LisAct* lis);
void FreeFIC(Fics *fic);
void FreeCPU(CPU  *cpu);

#endif