#include <windows.h>
#include "logging.h"
#include "dump.h"
#include "setupqueue.h"
#include "ctlserv.h"
#include "srvsocks.h"
#include "registrar.h"

int ServiceTryingToStop=0;
extern BOOL bDebug;

void __cdecl MainSock( void *pNotUsed ){
	struct sockaddr_in local,from;
	SOCKET LSock, CSock;
	WSADATA wsaData;
	int lonf,Ret;
	WSAStartup(0x202,&wsaData);
	local.sin_family=AF_INET;
	local.sin_addr.s_addr=INADDR_ANY;
	local.sin_port=htons(DEFAULT_SERVICE_PORT);
	LSock=socket(AF_INET,SOCK_STREAM,0);
	Ret=bind(LSock,(struct sockaddr*)&local,sizeof(local));
	Ret=listen(LSock,5);
	while(1){
		lonf=sizeof(from);
		CSock=accept(LSock,(struct sockaddr*)&from, &lonf);
		if( ServiceTryingToStop )
			break;
		SOCKET *vp=(SOCKET*)malloc(sizeof(SOCKET));
		*vp=CSock;
		_beginthread(ThreadSock,0,vp); //ThreadProc debe liberar vp y cerrar el sock
	}
	_endthread();
}

