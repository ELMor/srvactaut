

#include <Windows.h>
#include <lmcons.h>
#include <lmserver.h>

LPBYTE buf=NULL;
long eread,totale;

void copyDNS(char* dst,char* org){
	dst[0]=dst[1]=0;
	while( org[0] || org[1] ){
		dst[0]=org[0];
		dst[1]=org[1];
		dst+=2;
		org+=2;
	}
}

void ConvertUCtoS(char* dst, const char* org){
	while( org[0] || org[1] ){
		*dst=*org;
		org+=2;
		dst+=1;
	}
	*dst=0;
}