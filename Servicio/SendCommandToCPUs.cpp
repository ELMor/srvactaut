#include <windows.h>
#include "logging.h"
#include "dump.h"
#include "setupqueue.h"
#include "ctlserv.h"
#include "srvsocks.h"
#include "registrar.h"

extern BOOL bDebug;

int SendCommandToCPUs(LisAct* la, CPU *cpu, char *cmd){
	char *buf,*gbuf;
	int k,lon=0;
	Fics* fic=la->fic;

	gbuf=buf=(char*)malloc(SizeCommandBuffer);

	strcpy(buf,cmd);
	lon+=strlen(buf)+1;
	buf=gbuf+lon;

	BuildCommand(la->jobname,la->notifysrv,cpu->cname,cpu->cpus,fic,buf);
	lon+=strlen(buf)+1;
	buf=gbuf+lon;

	for(k=0;cpu->cpus && k<cpu->cpus->ncpu;k++){
		SOCKET sck=SendCommandToCPU(cpu->cpus->cpu[k]->cname,gbuf,lon);
		closesocket(sck);
		//Enviar log
		char msg[128];
		if( !stricmp(cmd,szQrBatch) )
			sprintf(msg,"Ordena a %s de que se actualice ha sido enviada.",
				cpu->cpus->cpu[k]->cname);
		else
			sprintf(msg,"Orden a %s de que actualice a sus hijas ha sido enviada.",
				cpu->cpus->cpu[k]->cname);
		SendMessageTo(la,"","",msg);
	}

	free(gbuf);
	return 1;
}

