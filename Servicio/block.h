
#ifndef __BLOCK_H__
#define __BLOCK_H__

#include <stdio.h>
#include <sys/stat.h>

//Separador de elementos de arbol
#define SEPCHAR	'\\'

class Nodo {
private:
	char			*nombre;		//Nombre del nodo
	long			tamCont;		//Tama�o de su contenido (para serializar);
	void*			contenido;		//Contenido
	Nodo*			padre;
	long			numHijos;		//Numero de hijos
	Nodo**			hijos;			//Hijos de este nodo
	long			trocea(char *s);
public:
					Nodo();
					~Nodo();
	void			setNom(char *s);
	char*s			getNom();
	void			setTamCont(long t);
	void			setCont(void *pc);
	void			setPadre(Nodo& p);
	Nodo*			getPadre();
	long			getNumHijos();
	void			addHijo(Nodo& nodo);
	Nodo*			addHijo(char *s);
	Nodo*			getNod(long index);
	Nodo*			busca(char *nom);
	void			serial(void*& p);
	void			serial(char* fname);
	void			deserial(void *p);
	void			deserial(char *fname);
};

#endif