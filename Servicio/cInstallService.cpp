#include "ctlserv.h"
#include "decompile.h"
#include "srvsocks.h"
#include "logging.h"

extern SERVICE_STATUS          ssStatus;       
extern SERVICE_STATUS_HANDLE   sshStatusHandle;
extern DWORD                   dwErr ;
extern BOOL                    bDebug ;
extern TCHAR                   szErr[256];

int CmdInstallService(char *pszNC, char *progName)
{
    SC_HANDLE   schService;
    SC_HANDLE   schSCManager;
    char szPath[512];
	HKEY k1,k2;
	unsigned long type,lon;

    if ( GetModuleFileName( NULL, szPath, 512 ) == 0 )
    {
        _tprintf(TEXT("Imposible instalar %s en %s - %s\n"), 
			TEXT(SZSERVICEDISPLAYNAME), 
			pszNC,
			GetLastErrorText(szErr, 256));
        return 0;
    }
	if( pszNC!=NULL && pszNC[0]!=0 ){
		char buf[1024];
		sprintf(buf,"copy \"%s\" \"\\\\%s\\ADMIN$\\SYSTEM32\" >NUL",szPath,pszNC);
		if( system(buf) ){
			printf("Fallo en la copia remota (%s).\n",pszNC);
			return 0;
		}
		if(ERROR_SUCCESS==RegConnectRegistry(pszNC,HKEY_LOCAL_MACHINE,&k1)){
			RegOpenKeyEx(k1,"SOFTWARE\\Microsoft\\Windows NT\\CurrentVersion",
				NULL,KEY_ALL_ACCESS,&k2);
			lon=512;
			RegQueryValueEx(k2,"SystemRoot",NULL,&type,(unsigned char*)szPath,&lon);
			RegCloseKey(k2);
			RegCloseKey(k1);
			sprintf(szPath+strlen(szPath),"\\System32\\%s",progName);
		}else{
			sprintf(szPath,"%s\\System32\\%s",getenv("SystemRoot"),progName);
		}
	}else{
		pszNC=NULL;
	}
    schSCManager = OpenSCManager(
                        pszNC,                  // machine (NULL == local)
                        NULL,                   // database (NULL == default)
                        SC_MANAGER_ALL_ACCESS   // access required
                        );
    if ( schSCManager )
    {
        schService = CreateService(
            schSCManager,               // SCManager database
            TEXT(SZSERVICENAME),        // name of service
            TEXT(SZSERVICEDISPLAYNAME), // name to display
            SERVICE_ALL_ACCESS,         // desired access
            SERVICE_WIN32_OWN_PROCESS |
			SERVICE_INTERACTIVE_PROCESS,// service type
            SERVICE_AUTO_START,			// start type
            SERVICE_ERROR_NORMAL,       // error control type
            szPath,                     // service's binary
            NULL,                       // no load ordering group
            NULL,                       // no tag identifier
            TEXT(SZDEPENDENCIES),       // dependencies
			NULL,						// Usr & Pwd (LocalSystem)
			NULL);

        if ( schService )
        {
            _tprintf(TEXT("%s instalado en %s.\n"), 
				TEXT(SZSERVICEDISPLAYNAME),
				TEXT(pszNC));
            CloseServiceHandle(schService);
        }
        else
        {
            _tprintf(TEXT("Fallo CreateService - %s\n"), GetLastErrorText(szErr, 256));
        }

        CloseServiceHandle(schSCManager);
		return 1;
    }
    else{
        _tprintf(TEXT("Fallo OpenSCManager - %s\n"), GetLastErrorText(szErr,256));
		return 0;
	}
}

