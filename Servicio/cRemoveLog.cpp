
#include "ctlserv.h"
#include "decompile.h"
#include "srvsocks.h"
#include "logging.h"

extern SERVICE_STATUS          ssStatus;       
extern SERVICE_STATUS_HANDLE   sshStatusHandle;
extern DWORD                   dwErr ;
extern BOOL                    bDebug ;
extern TCHAR                   szErr[256];

VOID CmdRemoveLog( char* cname, char *jn ){
	char xmsg[SizeXMitBuf];
	LisAct la;
	la.jobname=(char*)malloc(1);
	la.jobname[0]=0;
	la.cpu=(CPU*)malloc(sizeof(CPU));
	la.cpu->cname=strdup(cname);
	la.notifysrv=strdup(getenv("COMPUTERNAME"));
	la.fic=NULL;

	WSADATA wsaData;
	WSAStartup(0x202,&wsaData);
	strcat(strcpy(xmsg,PURGE_LOG_FILE),jn);
	SendMessageTo(&la,"","",xmsg);
	WSACleanup();
	
	free(la.notifysrv);
	free(la.cpu->cname);
	free(la.cpu);
	free(la.jobname);
}

