
#include "ctlserv.h"
#include "decompile.h"
#include "srvsocks.h"
#include "logging.h"

extern SERVICE_STATUS          ssStatus;       
extern SERVICE_STATUS_HANDLE   sshStatusHandle;
extern DWORD                   dwErr ;
extern BOOL                    bDebug ;
extern TCHAR                   szErr[256];

int CmdSendJob( char *fname ){
	//Lectura del archivo de �rdenes.
	FILE *fp=fopen(fname,"rb");
	if(!fp) { perror(fname);  return -1; }					//Error
	//Cargar en Memoria:
	struct stat st1;										//Tama�o?
	if(stat(fname,&st1)){ perror(fname); return -1; } 
	unsigned long lon=strlen(szRsBatch)+1 + st1.st_size+1;	//Comando+Contenido
	char *buf=(char*)malloc( lon );
	strcpy(buf,szRsBatch);
	fread(buf+strlen(buf)+1,1,st1.st_size,fp);
	buf[lon-1]=0;											//Debe terminar en '0'
	fclose(fp);
	char *cname;
	//Comprobar la sintaxis. Si hay alg�n error, detectarlo ahora.
	WSADATA wsaData;
	WSAStartup(0x202,&wsaData);
	{
		char *buf2=strdup(buf+strlen(buf)+1);
		LisAct la;
		int ret=Decompile(buf2,&la);
		free(buf2);
		if( !ret ){
			printf("Error de parsing en %s.",buf+strlen(buf)+1);
			free(buf);
			return -1;
		}
		//Inicializa logger con lista de CPUs que intervienen en el trabajo
		InitLogger( &la );
		//Comprobar que las m�quinas implicadas tienen el servicio ejecutandose
		CompruebaInstalaciones( &la, la.cpu );
		cname=strdup(la.cpu->cname);
		FreeAll( &la );
	}

	SendCommandToCPU(cname,buf,lon);
	WSACleanup();

	free(cname);
	free(buf);

	return 0;
}

