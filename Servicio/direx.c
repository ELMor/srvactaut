
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <direct.h>
#include <errno.h>

void mkdirex( char *dir ){
	int pos[512],npos=0,i,ret;
	char gdir[512],*s=gdir;
	strcpy(gdir,dir);
	//Obligar a que finalice en '\\'
	if( gdir[strlen(gdir)-1] != '\\' )
		strcat(gdir,"\\");
	while(s && *s){
		s=strchr(s,'\\');
		if(s && (s>gdir && *(s-1)!=':' && *(s-1)!='\\'))
			pos[npos++]=(s-gdir);
		if(s)
			s++;
	}
	for(i=npos-1;i>=0;i--){
		gdir[pos[i]]=0;
		ret=_mkdir(gdir);
		gdir[pos[i]]='\\';
		if(ret==0)
			break;
	}
	for(i++;i<npos;i++){
		gdir[pos[i]]=0;
		ret=_mkdir(gdir);
		gdir[pos[i]]='\\';
	}
}

