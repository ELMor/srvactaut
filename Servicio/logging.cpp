/*
 * Fichero logging.cpp
 */

#include <stdio.h>
#include <string.h>
#include <process.h>
#include <winsock.h>
#include <windows.h>
#include "ctlserv.h"
#include "logging.h"
#include "srvsocks.h"


extern int  ServiceTryingToStop;
extern DWORD dwErr ;
extern BOOL bDebug ;

FILE *fp;
static int  inUse=0;
static unsigned long nMensajes=0;

void 
SendMessageTo( LisAct* la, char* path, char* file, char * msg){
	char *buf=(char*)malloc(1024);
	strcpy(buf,szLogInfo);
	strcat(strcat(buf,la->jobname),				   "\\");
	gethostname(buf+strlen(buf),128);	strcat(buf,"\\");
	if(*file){
		strcat(buf,"Ficheros:\\");
		strcat(strcat(buf,path),"\\");
		strcat(strcat(buf,file),"\\");
	}else{
		strcat(buf,"Mensajes:\\");
	}
	strcat(buf,msg);

	closesocket(SendCommandToCPU(la->notifysrv,buf,strlen(buf),DEFAULT_NOTIFY_PORT));
	free(buf);
}

VOID 
AddToMessageLog(LPTSTR lpszMsg)
{
    TCHAR   szMsg[256];
    HANDLE  hEventSource;
    LPTSTR  lpszStrings[2];

    if ( !bDebug )
    {
        dwErr = GetLastError();
        hEventSource = RegisterEventSource(NULL, TEXT(SZSERVICENAME));
        _stprintf(szMsg, TEXT("%s error: %d"), TEXT(SZSERVICENAME), dwErr);
        lpszStrings[0] = szMsg;
        lpszStrings[1] = lpszMsg;
        if (hEventSource != NULL) {
            ReportEvent(hEventSource, // handle of event source
                EVENTLOG_ERROR_TYPE,  // event type
                0,                    // event category
                0,                    // event ID
                NULL,                 // current user's SID
                2,                    // strings in lpszStrings
                0,                    // no bytes of raw data
                (const char**)lpszStrings,          // array of error strings
                NULL);                // no raw data

            (VOID) DeregisterEventSource(hEventSource);
        }
    }
}

void 
ThreadSockMsg( void *lpv ){
	SOCKET *pCSock=(SOCKET*)lpv;
	char xBuf[SizeXMitBuf],*s=xBuf;
	unsigned long longitud,leidos;
	//Recibir el comando a ejecutar.
	recv(*pCSock,(char*)&longitud,sizeof(longitud),0);
	for(leidos=0;
		leidos<longitud;
		leidos+=recv(*pCSock,xBuf+leidos,SizeXMitBuf-leidos,0)
		)
		;	//Cuerpo vac�o
	xBuf[leidos]=0;
	//Lock del archivo de texto (MUY PRIMITIVO ESTO)
	while(inUse)
		Sleep(1000);
	inUse=1;
	if( strstr(xBuf,PURGE_LOG_FILE) ){
		char *jname=NULL;
		if( strlen(PURGE_LOG_FILE) < strlen(strstr(xBuf,PURGE_LOG_FILE)) )
			jname=strstr(xBuf,PURGE_LOG_FILE)+strlen(PURGE_LOG_FILE);
		if(!jname){
			fclose(fp);
			fp=fopen(LogFileName,"wt");
			nMensajes=0;
		}else{
			char tmpLogFileName[256],buffer[4096];
			FILE *fp2;
			fclose(fp);
			fp=fopen(LogFileName,"rt");
			strcat(strcpy(tmpLogFileName,LogFileName),".tmp");
			fp2=fopen(tmpLogFileName,"wt");
			nMensajes=0;
			while( fgets(buffer,4095,fp) && !feof(fp)){
				char *ini=strchr(buffer,'"'),*end=strchr(buffer,92);
				if( ini && end && strnicmp(jname,ini+1,end-ini-1)){
					fprintf(fp2,"%ld,",++nMensajes);				
					ini=strchr(buffer,',')+1;
					fprintf(fp2,"%s",ini);					
					fflush(fp2);
				}
			}
			fclose(fp);
			fclose(fp2);
			remove(LogFileName);
			rename(tmpLogFileName,LogFileName);
			fp=fopen(LogFileName,"at");
		}

	}else{
		fprintf(fp,"%ld,",++nMensajes);				//Numero de mensaje
		fprintf(fp,"\"%s\"\n",xBuf+strlen(szLogInfo));					
		fflush(fp);
	}
	//Unlock
	inUse=0;
	closesocket( *pCSock );
	free(lpv);
	_endthread();
}

void __cdecl 
MainSockMsg( void *pNotUsed ){
	struct sockaddr_in local,from;
	SOCKET LSock, CSock, *vp;
	WSADATA wsaData;
	int lonf,Ret;

	WSAStartup(0x202,&wsaData);
	local.sin_family=AF_INET;
	local.sin_addr.s_addr=INADDR_ANY;
	local.sin_port=htons(DEFAULT_NOTIFY_PORT);
	LSock=socket(AF_INET,SOCK_STREAM,0);
	Ret=bind(LSock,(struct sockaddr*)&local,sizeof(local));
	Ret=listen(LSock,5);
	while(1){
		lonf=sizeof(from);
		CSock=accept(LSock,(struct sockaddr*)&from, &lonf);
		if( ServiceTryingToStop )
			break;
		vp=(SOCKET*)malloc(sizeof(SOCKET));
		*vp=CSock;
		_beginthread(ThreadSockMsg,0,vp); //ThreadProc debe liberar vp y cerrar el sock
	}
	_endthread();
}

void 
InitMensajesSrvr(void){
	char buf[256];
	if((fp=fopen(LogFileName,"rt"))!=NULL){
		while( !feof(fp) ){
			fgets(buf,255,fp);
			sscanf(buf,"%ld",&nMensajes);
		}
		fclose(fp);
	}
	fp=fopen(LogFileName,"at");
	_beginthread(MainSockMsg,0,NULL);
	while(!ServiceTryingToStop)
		Sleep(10000);
	fclose(fp);
}

void RecInitLogger(LisAct* la, CPU* pcpu){
	char *buf=(char*)malloc(1024);
	int i;

	for(i=0;pcpu->cpus && i<pcpu->cpus->ncpu;i++){
		strcpy(buf,szLogInfo);
		strcat(strcat(buf,la->jobname),"\\");
		strcat(strcat(buf,pcpu->cpus->cpu[i]->cname),"\\");
		strcat(buf,"Mensajes:\\Aun no ha respondido. Esperando mensajes...");
		closesocket(SendCommandToCPU(la->notifysrv,buf,strlen(buf),
			DEFAULT_NOTIFY_PORT));
		if( pcpu->cpus->cpu[i]->cpus )
			RecInitLogger(la,pcpu->cpus->cpu[i]);
	}
	free(buf);
}

void InitLogger(LisAct* plis){
	RecInitLogger(plis,plis->cpu); 
}