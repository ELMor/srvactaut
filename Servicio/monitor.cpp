/*
 * Fichero monitor.cpp
 */


#include "monitor.h"
#include "registrar.h"

extern int ServiceTryingToStop;

void _cdecl MonitorLogon( void *p ){
	int lastlogon=0;		//No habia nadie trabajando. El servicio acaba de iniciarse	
	int currlogon=0;		//Recien iniciado el ordenador no hay nadie trabajando
	HKEY MasterKey;
	//Recien inicializado el servicio se procede a la actualización pendiente
	WSADATA wsaData;
	WSAStartup(0x202,&wsaData);
	ProcDelayedFic();
	do{
		//Monitorizar cada 10 segundos
		Sleep(10000);		
		if( ServiceTryingToStop)
			_endthread();
		if( ERROR_SUCCESS!=RegOpenKeyEx(HKEY_CURRENT_USER,NULL,0,KEY_ALL_ACCESS,&MasterKey) ){
			currlogon=0;	//No existe usuario
		}else{
			currlogon=1;	//Ahora si.
			RegCloseKey(MasterKey);
		}
		if( lastlogon==0 && currlogon==1 )	//Suponemos evento atrapado (Logout)
			ProcDelayedFic();
		lastlogon=currlogon;
	}while(1);
}
	