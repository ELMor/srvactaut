/*
 * Fichero monitor.h
 */


#ifndef __MONITOR_H__	
#define __MONITOR_H__

#include <stdio.h>
#include <stdlib.h>
#include <process.h>
#include <windows.h>

void __cdecl MonitorLogon( void *p );

#endif