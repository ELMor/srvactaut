/*
 * Fichero setupqueue.cpp
 */

#include "logging.h"
#include "CtlServ.h"
#include "setupqueue.h"
#include "configdir.h"
#include "registrar.h"

//Thread de Mensaje
void MensajeReinicio(void *p){
	while(1){
		MessageBox(NULL,
			"POR FAVOR, APAGE Y ENCIENDA LA MAQUINA "
			"UNA VEZ FINALIZADA LA TAREA QUE EST� EJECUTANDO,"
			" PARA QUE LAS APLICACIONES SEAN ACTUALIZADAS\n\n"
			"(Nota: Este mensaje se repetir� cada 10 minutos\n"
			"hasta que se reinicie el ordenador)"
			,
			"Mensaje del Servicio de Inform�tica de la CUN",
			MB_OK|MB_ICONEXCLAMATION|MB_SYSTEMMODAL|0x00200000L);
		Sleep(600000);
	}
}

//Una vez finalizada la copia en %TEMP%, abrir una cola para realizar las
// copias de los archivos que posiblemente est�n en uso.

UINT _stdcall TraceQueue(PVOID Context, UINT Notif, UINT p1, UINT p2){
	void **dummy=(void**)Context;
	void   *ContextForDefault=dummy[0];
	Fics   *fic=       (Fics*)dummy[1];
	LisAct *plis=	 (LisAct*)dummy[2];
	char buf[1024],msg[512],pfile[1024];
	int i=-1;

	pfile[0]=0;
	switch(Notif){
		case SPFILENOTIFY_FILEOPDELAYED:
		case SPFILENOTIFY_COPYERROR:
			strcpy(pfile,((PFILEPATHS)p1)->Target);
			break;
		case SPFILENOTIFY_ENDCOPY:
			ConvertUCtoS(pfile,((PFILEPATHS)p1)->Target);
			break;
		default:
			break;
	}
	if( pfile ){
		for(i=0;i<fic->carfic;i++){
			strcat(strcat(strcpy(buf,fic->path[i]),"\\"),fic->fname[i]);
			if(!stricmp(buf,pfile))
				break;
		}
	}
	msg[0]=0;
	switch(Notif){
		case SPFILENOTIFY_FILEOPDELAYED:
			fic->delay[i]=1;
			strcpy(msg,"Posponiendo la copia hasta Logout+Logon o Reinicio.");
			break;
		case SPFILENOTIFY_COPYERROR:
			sprintf(msg,"Notificacion de error en la copia (%s en %s).",
				((PFILEPATHS)p1)->Target,
				((PFILEPATHS)p1)->Source);
			break;
		case SPFILENOTIFY_ENDCOPY:
			strcpy(msg,"Archivo copiado.");
			break;
		default:
			break;
	}
	if(msg[0]){
		if(i==-1 || i>fic->carfic)
			SendMessageTo(plis,"","",msg);
		else
			SendMessageTo(plis,fic->path[i],fic->fname[i],
				msg);
	}
	//A la normal
	return (*SetupDefaultQueueCallback)(ContextForDefault,Notif,p1,p2);
}

int CopyQueue(LisAct* la ){
	HSPFILEQ fq=SetupOpenFileQueue();
	int i,NeedReboot=0;
	Fics* s=la->fic;
	PVOID Context;
	void *dummy[3];

	for(i=0;i<s->carfic;i++){
		s->delay[i]=0;
		if( s->downloaded[i] ){
			if(!SetupQueueCopy(fq,
				TEMP_DEST,s->path[i]+3,s->fname[i],
				NULL,NULL,s->path[i],NULL,
				SP_COPY_DELETESOURCE|
				SP_COPY_NODECOMP|
				SP_COPY_IN_USE_NEEDS_REBOOT)){
				sprintf(Msg,"Error SetupQueueCopy con %s->%s",s->path[i],s->fname[i]);
				SendMessageTo(la,s->path[i],s->fname[i],Msg);
			}
		}
	}
	Context=SetupInitDefaultQueueCallback(NULL);
	dummy[0]=Context;
	dummy[1]=(void*)s;
	dummy[2]=(void*)la;
	if(SetupCommitFileQueue(NULL,fq,TraceQueue,(void*)dummy))
		SendMessageTo(la,"","",
			"CommitFileQueue OK.");
	else
		SendMessageTo(la,"","",
			"Fall� la actualizaci�n CommitFileQueue");
	SetupCloseFileQueue( fq );
	SetupTermDefaultQueueCallback(Context);
	for(i=0;i<s->carfic;i++)
		if(s->delay[i])
			NeedReboot=1;
	if(NeedReboot){
		SendMessageTo(la,"","","NECESITA SER REINICIALIZADA!!!");
		_beginthread(MensajeReinicio,0,NULL);
	}
	return NeedReboot;
}

